package com.managment.demo.repository;

import com.managment.demo.model.Device;
import com.managment.demo.model.ModuleDbContext;
import com.managment.demo.repository.interfaces.ConnectedDeviceRepository;
import com.managment.demo.tables.ConnectedDevice;
import com.managment.demo.tables.records.ConnectedDeviceRecord;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public class JOOQConnectedDeviceRepository implements ConnectedDeviceRepository {

    private final DSLContext context;

    public JOOQConnectedDeviceRepository() {
        this.context = DSL.using(ModuleDbContext.getConnection(), SQLDialect.POSTGRES);
    }

    @Override
    public List<ConnectedDeviceRecord> GetAll() {
        return context.selectFrom(ConnectedDevice.CONNECTED_DEVICE)
                .fetch();
    }

    @Override
    public Device findByPhoneNumber(String phoneNumber) {

        ConnectedDeviceRecord record = context.selectFrom(ConnectedDevice.CONNECTED_DEVICE)
                .where(ConnectedDevice.CONNECTED_DEVICE.PHONE_NUMBER.eq(phoneNumber))
                .fetchOne();
        if(record == null) return null;
        else return record.into(Device.class);
    }

    @Override
    public Device update(Device device) {
        return context.update(ConnectedDevice.CONNECTED_DEVICE)
                .set(context.newRecord(ConnectedDevice.CONNECTED_DEVICE,device))
                .where(ConnectedDevice.CONNECTED_DEVICE.ID.eq(device.getId()))
                .returning()
                .fetchOptional()
                .orElseThrow(() -> new DataAccessException("Error updating entity: " + device.getId()))
                .into(Device.class);
    }

    @Override
    public Device insert(Device device) {
        return context.insertInto(ConnectedDevice.CONNECTED_DEVICE)
                .set(context.newRecord(ConnectedDevice.CONNECTED_DEVICE, device))
                .returning()
                .fetchOptional()
                .orElseThrow(() -> new DataAccessException("Error inserting entity: " + device.getId()))
                .into(Device.class);
    }
}
