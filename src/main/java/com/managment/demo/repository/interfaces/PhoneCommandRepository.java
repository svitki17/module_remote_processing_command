package com.managment.demo.repository.interfaces;

import com.managment.demo.model.DeviceCommandDTO;
import com.managment.demo.model.Enums.CommandState;
import com.managment.demo.tables.records.PhoneCommandRecord;


public interface PhoneCommandRepository {
    DeviceCommandDTO findByPhoneNumberAndCommand(String number, String command);

    DeviceCommandDTO findByPhoneNumberAndState(String number, String state);

    DeviceCommandDTO getCommandHighestPriority(String number);

    DeviceCommandDTO insert(DeviceCommandDTO device);

    DeviceCommandDTO update(DeviceCommandDTO device);

    DeviceCommandDTO delete(DeviceCommandDTO device);

}
