package com.managment.demo.repository.interfaces;

import com.managment.demo.model.Device;
import com.managment.demo.tables.ConnectedDevice;
import com.managment.demo.tables.records.ConnectedDeviceRecord;

import java.util.List;

public interface ConnectedDeviceRepository {
    List<ConnectedDeviceRecord> GetAll();

    Device findByPhoneNumber(String phoneNumber);

    Device update(Device device);

    Device insert(Device device);
}
