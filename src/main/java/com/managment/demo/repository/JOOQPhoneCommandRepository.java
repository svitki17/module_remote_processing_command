package com.managment.demo.repository;

import com.managment.demo.model.Device;
import com.managment.demo.model.DeviceCommandDTO;
import com.managment.demo.model.Enums.CommandState;
import com.managment.demo.model.ModuleDbContext;
import com.managment.demo.repository.interfaces.PhoneCommandRepository;

import com.managment.demo.tables.ConnectedDevice;
import com.managment.demo.tables.PhoneCommand;
import com.managment.demo.tables.records.PhoneCommandRecord;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;

@Repository
public class JOOQPhoneCommandRepository implements PhoneCommandRepository {

    private final DSLContext context;

    public JOOQPhoneCommandRepository() {
        this.context = DSL.using(ModuleDbContext.getConnection(), SQLDialect.POSTGRES);
    }
    @Override
    public DeviceCommandDTO findByPhoneNumberAndState(String number, String state) {
        PhoneCommandRecord phoneCommandRecord = context.selectFrom(PhoneCommand.PHONE_COMMAND)
                .where(PhoneCommand.PHONE_COMMAND.PHONE_NUMBER.eq(number).and(PhoneCommand.PHONE_COMMAND.STATE.eq(state)))
                .fetchOne();
        if(phoneCommandRecord == null) return null;

        return phoneCommandRecord.into(DeviceCommandDTO.class);
    }

    @Override
    public DeviceCommandDTO getCommandHighestPriority(String number) {
        PhoneCommandRecord phoneCommandRecord = context.selectFrom(PhoneCommand.PHONE_COMMAND)
                .where(PhoneCommand.PHONE_COMMAND.PHONE_NUMBER.eq(number).and(PhoneCommand.PHONE_COMMAND.STATE.ne(CommandState.COMPLETED.name())))
                .orderBy(PhoneCommand.PHONE_COMMAND.PRIORITY.asc())
                .fetchOne();
        if(phoneCommandRecord == null) return null;

        return phoneCommandRecord.into(DeviceCommandDTO.class);
    }

    @Override
    public DeviceCommandDTO findByPhoneNumberAndCommand(String number, String command) {
        PhoneCommandRecord phoneCommandRecord = context.selectFrom(PhoneCommand.PHONE_COMMAND)
                .where(PhoneCommand.PHONE_COMMAND.PHONE_NUMBER.eq(number).and(PhoneCommand.PHONE_COMMAND.COMMAND.eq(command)))
                .fetchOne();
        if(phoneCommandRecord == null) return null;

        return phoneCommandRecord.into(DeviceCommandDTO.class);
    }

    @Override
    public DeviceCommandDTO insert(DeviceCommandDTO device) {
        return context.insertInto(PhoneCommand.PHONE_COMMAND)
                .set(context.newRecord(PhoneCommand.PHONE_COMMAND, device))
                .returning()
                .fetchOptional()
                .orElseThrow(() -> new DataAccessException("Error inserting entity: " + device.getId()))
                .into(DeviceCommandDTO.class);
    }

    @Override
    public DeviceCommandDTO update(DeviceCommandDTO device) {
        return context.update(PhoneCommand.PHONE_COMMAND)
                .set(context.newRecord(PhoneCommand.PHONE_COMMAND,device))
                .where(PhoneCommand.PHONE_COMMAND.ID.eq(device.getId()))
                .returning()
                .fetchOptional()
                .orElseThrow(() -> new DataAccessException("Error updating entity: " + device.getId()))
                .into(DeviceCommandDTO.class);
    }

    @Override
    public DeviceCommandDTO delete(DeviceCommandDTO device) {
        return null;
    }
}
