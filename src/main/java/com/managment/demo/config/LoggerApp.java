package com.managment.demo.config;

import org.slf4j.LoggerFactory;

public class LoggerApp<T>{
    public final T inst;
    public final org.slf4j.Logger log;

    public LoggerApp(T inst) {
        this.inst = inst;
        this.log = LoggerFactory.getLogger(inst.getClass());
    }
}
