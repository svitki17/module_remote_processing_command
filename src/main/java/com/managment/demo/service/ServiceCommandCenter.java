package com.managment.demo.service;

import com.google.gson.Gson;
import com.google.protobuf.util.JsonFormat;
import com.managment.*;
import com.managment.demo.model.DeviceCommandDTO;
import com.managment.demo.model.json.FileSystemJson;
import com.managment.demo.model.json.PhoneRequestJson;
import com.managment.demo.repository.JOOQPhoneCommandRepository;
import io.grpc.stub.StreamObserver;
import lombok.SneakyThrows;
import net.devh.boot.grpc.server.service.GrpcService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.UUID;

@GrpcService
@Service
public class ServiceCommandCenter extends CommandCenterGrpc.CommandCenterImplBase {
    private static final Logger log = LoggerFactory.getLogger(ServiceCommandCenter.class);
    private final JOOQPhoneCommandRepository repository;

    public ServiceCommandCenter(JOOQPhoneCommandRepository repository) {
        this.repository = repository;
    }

    @Override
    public void getCommand(PhoneRequest request, StreamObserver<PhoneResponse> responseObserver) {
        DeviceCommandDTO phoneCommand = repository.findByPhoneNumberAndCommand(request.getPhoneNumber(),request.getCommand().name());
        if(phoneCommand == null){
            log.info("->There are no commands available for the device [phoneNumber={}]",request.getPhoneNumber());
            responseObserver.onNext(PhoneResponse.newBuilder()
                    .setCommand(request.getCommand())
                    .setPhoneNumber(request.getPhoneNumber()).build());
            responseObserver.onCompleted();
            return;
        }
        responseObserver.onNext(BuildPhoneGrpc(phoneCommand));
        responseObserver.onCompleted();
    }

    @Override
    public void getOneCommand(PhoneRequest request, StreamObserver<PhoneResponse> responseObserver) {
        DeviceCommandDTO phoneCommand = repository.getCommandHighestPriority(request.getPhoneNumber());
        if(phoneCommand == null){
            log.info("->There are no commands available for the device [phoneNumber={}]",request.getPhoneNumber());
            responseObserver.onNext(PhoneResponse.newBuilder().getDefaultInstanceForType());
            responseObserver.onCompleted();
            return;
        }
        responseObserver.onNext(BuildPhoneGrpc(phoneCommand));
        responseObserver.onCompleted();
    }

    public PhoneResponse BuildPhoneGrpc(DeviceCommandDTO phoneRecord){
        Gson gson = new Gson();
        PhoneRequestJson phoneRequestJson = gson.fromJson(phoneRecord.getRequest(), PhoneRequestJson.class);
        FileSystem.Builder fileSystem = FileSystem.newBuilder();

        if(phoneRequestJson != null && phoneRequestJson.fileSystem != null)
            fileSystem.setTransitionPath(phoneRequestJson.fileSystem.transitionPath);
        fileSystem.build();
        return PhoneResponse.newBuilder()
                .setCommand(Command.valueOf(phoneRecord.getCommand()))
                .setPhoneNumber(phoneRecord.getPhoneNumber())
                .setFileSystem(fileSystem)
                .setJson(phoneRecord.getResult()== null ? "": phoneRecord.getResult())
                .setIsCommandAvailable(true)
                .setState(StateCommand.valueOf(phoneRecord.getState()))
                .build();
    }


    @SneakyThrows
    @Override
    public void setProcessingCommand(PhoneRequest request, StreamObserver<PhoneResponse> responseObserver) {

        DeviceCommandDTO deviceCommand = repository.findByPhoneNumberAndCommand(request.getPhoneNumber(),request.getCommand().name());

        if(deviceCommand == null){
            deviceCommand = new DeviceCommandDTO();
            deviceCommand.setCommand(request.getCommand().name());
            deviceCommand.setState(StateCommand.ACCEPTED.name());
            deviceCommand.setRequest(JsonFormat.printer().print(request));
            deviceCommand.setGuid(UUID.randomUUID().toString());
            deviceCommand.setPhoneNumber(request.getPhoneNumber());
            repository.insert(deviceCommand);
        }else{
            if(request.getState() == StateCommand.COMPLETED)
                deviceCommand.setResult(JsonFormat.printer().print(request));
            else
                deviceCommand.setRequest(JsonFormat.printer().print(request));
            deviceCommand.setState(request.getState().name());
            repository.update(deviceCommand);
        }
        log.info("-->Installing the command for number={} -->old=[command={},state={}] --> new=[command={},state={}]", request.getPhoneNumber(),request.getCommand(),request.getState(), deviceCommand.getCommand(),deviceCommand.getState());
        responseObserver.onNext(BuildPhoneGrpc(deviceCommand));
        responseObserver.onCompleted();

    }
}
