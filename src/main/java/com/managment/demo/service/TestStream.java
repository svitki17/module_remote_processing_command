package com.managment.demo.service;

import com.google.protobuf.util.JsonFormat;
import com.managment.Command;
import com.managment.PhoneRequest;
import com.managment.PhoneResponse;
import com.managment.StateCommand;
import com.managment.demo.config.LoggerApp;
import com.managment.demo.model.DeviceCommandDTO;
import com.managment.demo.repository.JOOQConnectedDeviceRepository;
import com.managment.demo.repository.JOOQPhoneCommandRepository;
import io.grpc.stub.StreamObserver;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

public class TestStream  implements StreamObserver<PhoneResponse> {

    private final StreamObserver<PhoneRequest> phoneResponseStreamObserver;
    private static final Logger log = LoggerFactory.getLogger(TestStream.class);
    private final JOOQPhoneCommandRepository commandRepository;


    public TestStream(StreamObserver<PhoneRequest> phoneResponseStreamObserver, JOOQPhoneCommandRepository commandRepository) {
        this.phoneResponseStreamObserver = phoneResponseStreamObserver;
        this.commandRepository = commandRepository;
    }

    @SneakyThrows
    @Override
    public void onNext(PhoneResponse response) {
        DeviceCommandDTO deviceCommand = commandRepository.findByPhoneNumberAndCommand(response.getPhoneNumber(),response.getCommand().name());
        log.info("-> The response to the command came [command={},state={},phoneNumber={}]", response.getCommand().name(), response.getState().name(),response.getPhoneNumber());
        if(response.getCommand() == Command.GET_FILES_LIST && response.getState() == StateCommand.COMPLETED){
            if(deviceCommand == null){
                deviceCommand = new DeviceCommandDTO();
                deviceCommand.setCommand(response.getCommand().name());
                deviceCommand.setState(StateCommand.ACCEPTED.name());
                deviceCommand.setResult(JsonFormat.printer().print(response));
                deviceCommand.setGuid(UUID.randomUUID().toString());
                deviceCommand.setPhoneNumber(response.getPhoneNumber());
                commandRepository.insert(deviceCommand);
            }else{
                deviceCommand.setResult(JsonFormat.printer().print(response));
                deviceCommand.setState(response.getState().name());
                commandRepository.update(deviceCommand);
            }
            this.phoneResponseStreamObserver.onNext(PhoneRequest.getDefaultInstance());
        }

    }

    @Override
    public void onError(Throwable t) {
        System.out.println("--->test OnError");
    }

    @Override
    public void onCompleted() {
        System.out.println("--->test onCompleted");
    }
}
