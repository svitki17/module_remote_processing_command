package com.managment.demo.service;

import com.managment.*;
import com.managment.demo.model.Device;
import com.managment.demo.repository.JOOQConnectedDeviceRepository;
import com.managment.demo.repository.JOOQPhoneCommandRepository;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;
import org.springframework.stereotype.Service;

@GrpcService
@Service
public class ServicePhoneCenter extends PhoneCenterGrpc.PhoneCenterImplBase {

    private final JOOQConnectedDeviceRepository connectedDeviceRepository;
    private final JOOQPhoneCommandRepository commandRepository;

    public ServicePhoneCenter(JOOQConnectedDeviceRepository connectedDeviceRepository, JOOQPhoneCommandRepository commandRepository) {
        this.connectedDeviceRepository = connectedDeviceRepository;
        this.commandRepository = commandRepository;
    }

    @Override
    public void getConnectedDevice(DeviceConnectedRequest request, StreamObserver<DeviceConnectedResponse> responseObserver) {

        connectedDeviceRepository.GetAll().forEach(x->{
            DeviceConnectedResponse deviceConnectedResponse = DeviceConnectedResponse.newBuilder()
                    .setCity(x.getCity())
                    .setPhoneNumber(x.getPhoneNumber())
                    .setOperator(x.getOperator())
                    .setNetworkType(x.getNetworkType())
                    .setIp(x.getIp())
                    .setCountry(x.getCountry())
                    .setZip(x.getZip())
                    .setRegion(x.getRegion())
                    .setStateConnection(x.getStateConnection())
                    .build();
            responseObserver.onNext(deviceConnectedResponse);
        });

        responseObserver.onCompleted();
    }

    @Override
    public StreamObserver<PhoneResponse> bidirectionalTest(StreamObserver<PhoneRequest> responseObserver) {
        return new TestStream(responseObserver, commandRepository);
    }

    @Override
    public void synchronization(DeviceConnectedRequest request, StreamObserver<DeviceConnectedResponse> responseObserver) {
        Device deviceRecord = connectedDeviceRepository.findByPhoneNumber(request.getPhoneNumber());

        if(deviceRecord != null)
            deviceRecord = connectedDeviceRepository.update(BuildGrpcToModel(deviceRecord,request,"CONNECTION"));
        else
            deviceRecord = connectedDeviceRepository.insert(BuildGrpcToModel(null,request,"CONNECTION"));
        DeviceConnectedResponse d = DeviceConnectedResponse.newBuilder()
                .setStateConnection(deviceRecord.getStateConnection())
                .setPhoneNumber(deviceRecord.getPhoneNumber()).build();
        responseObserver.onNext(d);
        responseObserver.onCompleted();

    }


    public Device BuildGrpcToModel(Device device, DeviceConnectedRequest request,String stateConnection){
        if(device == null)
            device = new Device();
        device.setCity(request.getCity());
        device.setCountry(request.getCountry());
        device.setOperator(request.getOperator());
        device.setRegion(request.getRegion());
        device.setPhoneNumber(request.getPhoneNumber());
        device.setIp(request.getIp());
        device.setZip(request.getZip());
        device.setNetworkType(request.getNetworkType());
        device.setStateConnection(stateConnection);
        return device;
    }
}
