package com.managment.demo.model;

import lombok.Data;

@Data
public class DeviceCommandDTO {

    private Long Id;

    private String phoneNumber;

    private String command;

    private String state;
    private String result;

    private String request;

    private String guid;
}
