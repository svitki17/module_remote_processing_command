package com.managment.demo.model;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;

import java.sql.Connection;
import java.sql.DriverManager;

public class ModuleDbContext {


    @Value("${db.user.name}")
    private static String userName;
    @Value("${db.user.password}")
    private static String password;
    @Value("${db.url}")
    private static String url;
    @SneakyThrows
    public static Connection getConnection(){
        return DriverManager.getConnection(url, userName, password);
    }
}
