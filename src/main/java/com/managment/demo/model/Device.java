package com.managment.demo.model;

import lombok.Data;

@Data
public class Device {
    private Integer Id;
    private String city;
    private String country;
    private String region;
    private String ip;
    private String zip;
    private String query;

    private String operator;

    private String phoneNumber;

    private String networkType;

    private String stateConnection;
}
