package com.managment.demo.model.Enums;

public enum CommandState {
    WAITING,

    ACCEPTED,

    PROCESSED,

    COMPLETED
}
